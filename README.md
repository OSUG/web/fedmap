# fedmap

## Vocabulaire

  * **chaine** : suite de caractères encodés utf-8
  * **chaine vide** : chaine ne contenant aucun caractère.
  * **nombre décimal** : chaine contenant des chiffres et éventuellement un point "."
  * **type** : un entier associé à un point sur la carte permettant de définir des catégories de points
  * **ticket** : une chaine de caractères aléatoire unique sans signification servant de preuve. 
  * **session** : ensemble de requêtes HTTP provenant d'un même utilisateur. L'association entre les différentes requêtes est possible grâce à l'enregistrement d'un identifiant unique (chaine de caractère aléatoire unique sans signification) dans un cookie. 
  * **email** : chaine de carcatère de la forme nom@domaine 
  * **latitude**, **longitude** : Décimaux relatifs. Coordonnées géographiques exprimées en degrés d'angle.
  * **service** : point d'accès au système via le protocole HTTP identifié par un chemin d'URL 
  * **chemin** : chaine de caractère apparaissant à la fin d'une URL commençant par "/"
  * **methode** : Verbe du protocole HTTP permettant de qualifier un requête.
  * **données utilisateur** : données associées à un point de la carte qui ne sont pas utilisées par l'application.

## Fonctionnement

FedMap se compose d'une API HTTP REST. C'est à dire d'un ensemble URL d'accès permettant d'interagir avec
l'application via HTTP.

Les service permettent d'accéder aux données de la carte, d'enregistrer de nouvelles coordonnées et des données
associées ou de les modifier, d'envoyer un message à une adresse email associée à des coordonnées sur la carte.

L'adresse email constitue un identifiant unique et le système de messagerie constitue le moyen de s'identifier
en confirmant son adresse via un lien envoyé par messagerie.

Toute l'interface destinée à l'utilisateur (pages HTML) est laissée aux soins du gestionnaire du site web.

## Procédure d'identification.

Avant toute interaction avec l'application un utilisateur doit s'identifier grâce à son adresse email.

  - L'utilisateur soumet son adresse via un formulaire.
  - Le serveur envoie un message à l'adresse contenant un ticket unique.
  - L'utilisateur clique sur le lien dans le message.
  - Le serveur vérifie le ticket et en cas de succès ouvre un session.

  **machine utilisateur**                         **site / serveur**                      **boite email utilisateur**

                                   HTTP:GET
      formulaire HTML       <--------------------- formulaire HTML
    entre adresse email
                                   HTTP:POST                                 SMTP
        page HTML           <--------------------> création ticket
                                                    envoie email    --------------------> 
                                                                                           email : lien avec ticket
      consulte boîte                                 IMAP/POP
      lecture email         <-------------------------------------------------------------
    clique sur le lien
      avec ticket
                                   HTTP:GET        vérifie ticket
       page HTML            <--------------------- création cookie session
                                                   

## Services

### Approbation d'enregistrement.

Service permetant d'approuver l'enregistrement d'un nouveau point ou d'une modification soumis par un utilisateur. 

  * **chemin** : /approve
  * **methode** : GET
  * **données** :
    * ticket d'approbation passé dans l'URL (/approve:_ticket_)
  * **retours** :
    * redirection temporaire (HTTP 302 found)

### Accès aux données utilisateur.

  * **chemin** : /data
  * **méthode** : GET
  * **données** : _aucune_
  * **retours** :
    * chaine JSON contenant les données associées à la session.

### Accès aux données géographiques.

  * **chemin** : /geojson
  * **méthode** : GET
  * **données** :
    * (facultative) type de point passé dans l'URL (/geojson:_type_) 
  * **retours** :
    * chaine GEOJSON contenant les coordonnées enregistrées. 

### Envoi de message à un contact.

  * **chemin** : /message
  * **méthode** : POST
  * **données** :
  	* place_id : entier clé primaire de l'entrée dans la base de donnée correspondant au point
  	* au choix de l'utilisateur. les données sont passées au 
  * **retours** :
    * code ou redirection temporaire (HTTP 302 found)

### Enregistrement d'un emplacement sur la carte ou mise à jour.

  * **chemin** : /record
  * **méthode** : POST
  * **données** :
    * **type** : type des coordonnées (entier définissant une catégorie)
    * **latitude** : [nombre décimal] degrés d'angle ou [chaine vide] refuser d'apparaître sur la carte
    * **longitude** : [nombre décimal] degrés d'angle ou [chaine vide] refuser d'apparaître sur la carte
    * **forbidden** (facultatif) : [chaine] URL de redirection en cas d'interdiction (pas de session)
    * **fail** (facultatif) : [chaine] URL de redirection en cas d'échec (type inexistant)
    * **success** (facultatif) : [chaine] URL de redirection en cas de succés.
    * _données diverses_ : données supplémentaires associées aux coordonnées au choix du concepteur du formulaire.
  * **retours** :
    * code ou redirection temporaire (HTTP 302 found)

### Vérification d'ouverture de session.

  * **chemin** : /session
  * **méthode** : GET
  * **données** : _aucune_
  * **retours** :
    * chaine JSON "true" ou "false" 

### Demande d'identification par email

  * **chemin** : /request
  * **méthode** : POST
  * **données** :
  * **retours** :

### Validation d'adresse email

Service permettant de valider une adresse email grâce à un ticket unique envoyé à cette adresse.

  * **chemin** : /validate
  * **méthode** : GET
  * **données** :
    * ticket passé dans l'URL (/validate:_ticket_)
  * **retours** :

### Page de requête

Avant de


## Configuration apache

<VirtualHost *:80>
	ServerName monsite.fr
	DocumentRoot /var/www/monsite/
	AliasMatch ^/fedmap/.*$ /chemin/code/http.php
	SetEnv CONFIG_PATH /chemin/config/pour/monsite/config.php
	<IfModule !mod_php7.c>
		<IfModule proxy_fcgi_module>
			<IfModule setenvif_module>
				SetEnvIfNoCase ^Authorization$ "\(.+\)" HTTP_AUTHORIZATION=$1
			</IfModule>
			<FilesMatch ".+\.php$">
	   			SetHandler "proxy:unix:/run/php/fedmap.sock|fcgi://localhost"
			</FilesMatch>
		</IfModule>
	</IfModule>
</VirtualHost>

