<?php

namespace map;

class _bootstrap
{
	protected static $accept = [
		'|^fr|i' => 'fr_FR',
		'|^en|i' => 'en_US'
	];
	protected static $_errors = [
		E_USER_ERROR => 'user error',
		E_USER_WARNING => 'warning',
		E_USER_NOTICE => 'notice'
	];
	protected static $configuration = [];
	protected static $db = null;
	public static $root;
	public static $doc;
	public static $lang;

	public static function load(string $class)
	{
		if (is_file($path = sprintf('%s/src/%s.class.php',self::$root,strtr($class,'\\',DIRECTORY_SEPARATOR))))
			require $path;
		else
			throw new \Exception(sprintf('Unable to find class "%s"',$class),1);
	}

	public static function debug(string $message, string ...$args)
	{
		error_log(sprintf('-------------------%s%s%s',PHP_EOL,date('d/m/Y H:i:s'),PHP_EOL),3,self::get('BACKUPPC_CONF_PATH').'/debug.log');
		error_log(vsprintf($message,$args).PHP_EOL,3,self::get('BACKUPPC_CONF_PATH').'/debug.log');
	}

	public static function log_error(string $message, string ...$args)
	{
		vprintf($message,$args);
//		error_log(vsprintf($message,$args),3,self::get('BACKUPPC_TOP_PATH').'/'.self::LOG_FILE);
	} 

	public static function exception_handler($exception)
	{
		$message = PHP_EOL.'--------------------'.PHP_EOL;
		$message .= sprintf('date : %s'.PHP_EOL,date('Y/m/d H:i:s'));
		$message .= sprintf('EXCEPTION : "%s"',$exception->getMessage());
		$message .= sprintf('%sin file "%s" line %s',PHP_EOL,$exception->getFile(),$exception->getLine());
		foreach ($exception->getTrace() as $trace)
			$message .= sprintf('%scalled by "%s" line %s',PHP_EOL,$trace['file'] ?? 'unknown',$trace['line'] ?? 'unknown');
		$message .= PHP_EOL;
		self::log_error($message);
	}

	public static function error_handler($number, $message, $file, $line)
	{
		$message = '--------------------'.PHP_EOL;
		$message .= sprintf('date : %s'.PHP_EOL,date('Y/m/d H:i:s'));
		$message .= sprintf('%s : %s'.PHP_EOL,self::$_errors[$number] ?? 'UNKNOWN ERROR',$message);
		$message .= sprintf('%s on line %d'.PHP_EOL,$file,$line);
		self::log_error($message);
		return false;
	}


	public static function get(string $key) : ?string
	{
		return self::$configuration[$key] ?? null;
	}

	public static function configuration() : array
	{
		return self::$configuration;
	}

	public static function db() : db
	{
		if (self::$db === null)
			self::$db = new db(self::get('DB_HOST'),self::get('DB_USER'),self::get('DB_PASS'),self::get('DB_BASE'));
		return self::$db;
	}

	public static function init(string $document_root, string $config_path)
	{
		self::$root = __DIR__;
		self::$doc = $document_root;
/*		ini_set('display_errors',1);
		error_reporting(E_ALL);*/
		spl_autoload_register('map\_bootstrap::load',true,true);
		set_exception_handler('map\_bootstrap::exception_handler');
//		set_error_handler('backup\_bootstrap::error_handler');
		date_default_timezone_set('Europe/Paris');
		self::$lang = 'en_US';
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
		{
			$prefs = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
			foreach ($prefs as $lang)
			{
				foreach (self::$accept as $regex => $value)
				{
					if (preg_match($regex,$lang))
					{
						self::$lang = $value;
						break 2;
					}
				}
			}
		}
		setlocale(LC_ALL,self::$lang.'.UTF-8');
		self::$configuration = require $config_path;
	}
}
