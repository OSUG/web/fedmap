<?php

namespace fedmap;

use oTools\mysql\connection,
	oTools\mysql\result;

class db extends connection
{
	const TICKET_TTL = 3600;
	const QUOTA_TTL = 36000;
	const QUOTA_MAX = 10;

	protected $base;
	public $last_query;

	public function __construct(string $address, string $login, string $password, string $base, string $ssl_key = null, string $ssl_cert = null, string $ssl_ca = null)
	{
		parent::__construct($address,$login,$password,$ssl_key,$ssl_cert,$ssl_ca);
		$this->base = $this->base($base);
	}

	protected function _clean()
	{
		$this->base->request()
			->sql('DELETE FROM /map_tickets WHERE /creation < :ut')
			->sbinds(time() - _::get('ticket-ttl'))
			->exec();
	}

	public function create_ticket(array $data) : string
	{
		$this->_clean();
		$ticket = hash('sha256',uniqid('',true));
		$success = $this->base->request()
			->sql('INSERT INTO /map_tickets (/ticket,/creation,/data) VALUES (:ticket,:time,:data)')
			->sbinds($ticket,time(),serialize($data))
			->exec();
		if ($success === false)
			throw new exception('erreur création de ticket');
		return $ticket;
	}

	public function verify_ticket(string $ticket) : array
	{
		$this->_clean();
		$result = $this->base->request()
			->sql('SELECT /data FROM /map_tickets WHERE /ticket = :ticket')
			->sbinds($ticket)
			->exec();
		if (count($result) !== 1)
			return [];
		$this->base->request()
			->sql('DELETE FROM /map_tickets WHERE /ticket = :ticket')
			->sbinds($ticket)
			->exec();
		return unserialize($result[0]['data']);
	}

	public function add_place(int $type, string $email, ?int $latitude, ?int $longitude, bool $valid, array $data)
	{
		$this->base->request()
			->sql('INSERT INTO /map_places (/type, /email, /latitude, /longitude, /valid, /data) VALUES (:type, :mail, :lat, :lng, :val, :data) ON DUPLICATE KEY UPDATE /type = :type, /latitude = :lat, /longitude = :lng, /data = :data')
			->sbinds($type,$email,$latitude,$longitude,$valid,json_encode($data))
			->exec();
	}

	public function approve_place(int $id)
	{
		$this->base->request()
			->sql('UPDATE /map_places SET /valid = 1 WHERE /place_id = :id')
			->sbinds($id)
			->exec();
	}

	public function get_places(string $type = null) : result
	{
		$sql = 'SELECT /place_id, /type, /latitude, /longitude, /data FROM /map_places WHERE /valid = 1 AND /latitude IS NOT NULL AND /longitude IS NOT NULL';
		if ($type === null)
			return $this->base->request()->sql($sql)->exec();
		$sql .= ' AND /type = :type';
		return $this->base->request()->sql($sql)->sbinds($type)->exec();
	}

	public function get_place_from_email(string $email) : ?array
	{
		$result = $this->base->request()
			->sql('SELECT /type, /latitude, /longitude, /data FROM /map_places WHERE /email = :email')
			->sbinds($email)
			->exec();
		return $result[0] ?? null;
	}

	public function get_place(string $id) : ?array
	{
		$result = $this->base->request()
			->sql('SELECT /email, /type, /latitude, /longitude, /data FROM /map_places WHERE /place_id = :id')
			->sbinds($id)
			->exec();
		return $result[0] ?? null;
	}
}