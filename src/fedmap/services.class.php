<?php

namespace fedmap;

use DateTimeImmutable,
	oTools\template,
	oTools\http\server,
	oTools\session,
	oTools\session\handlers\mysql,
	oTools\session\identifiers\cookie;

class services
{
	protected static $services = [
		'http' => [
			'get' => [
				'/validate' => ['_service_validate','text/html; Content-Type: utf-8'],
				'/approve' => ['_service_approve','text/html; Content-Type: utf-8'],
				'/geojson' => ['_service_geojson','application/json'],
				'/data' => ['_service_data','application/json'],
				'/infos' => ['_service_infos','text/html; Content-Type: utf-8'],
				'/session' => ['_service_session','application/json']
			],
			'post' => [
				'/request' => ['_service_request','application/json'],
				'/record' => ['_service_record','application/json'],
				'/message' => ['_service_message','application/json'],
				'/session' => ['_service_session_redirect','application/json']
			]
		],
		'https' => [
			'get' => [
				'/validate' => ['_service_validate','text/html; Content-Type: utf-8'],
				'/approve' => ['_service_approve','text/html; Content-Type: utf-8'],
				'/geojson' => ['_service_geojson','application/json'],
				'/data' => ['_service_data','application/json'],
				'/infos' => ['_service_infos','text/html; Content-Type: utf-8'],
				'/session' => ['_service_session','application/json']
			],
			'post' => [
				'/request' => ['_service_request','application/json'],
				'/record' => ['_service_record','application/json'],
				'/message' => ['_service_message','application/json'],
				'/session' => ['_service_session_redirect','application/json']
			]
		]
	];

	protected static $language = 'en_US';
	protected static $session;

	protected static function _send_json($data)
	{
		echo json_encode($data).PHP_EOL;
	}

	protected static function _cond_redirect(int $status, ?string $url, string $message = '', string ...$args)
	{
		server::status($status);
		if ($url !== null)
			return server::found($url);
		echo json_encode(vsprintf($message,$args)).PHP_EOL;
	}

	protected static function _error(int $status, string $message, string ...$args)
	{
		server::status($status);
		self::_send_json(['name' => 'error', 'data' => vsprintf($message,$args)]);
	}

	protected static function _message(string $type, string $message, string ...$args)
	{
		self::_send_json(['name' => $type, 'data' => vsprintf($message,$args)]);
	}

	protected static function _current_ip()
	{
		return $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['REMOTE_ADDR'];
	}

	protected static function _service_geojson(string $type = null)
	{
		$data = [
			'type' => 'FeatureCollection',
			'features' => []
		];
		$places = _::db()->get_places($type);
		foreach ($places as $place)
		{
			$properties = json_decode($place['data'],true);
			$properties['id'] = $place['place_id'];
			$properties['type'] = $place['type'];
			$data['features'][] = [
				'type' => 'Feature',
				'geometry' => [
					'type' => 'Point',
					'coordinates' => [
						sprintf('%0.3F',((int)$place['longitude'] / 1000)),
						sprintf('%0.3F',((int)$place['latitude'] / 1000))
					]
				],
				'properties' => $properties
			];
		}
		self::_send_json($data);
	}

	protected static function _service_data()
	{
		if (! self::$session->exists())
			return self::_error(403,'forbidden');
		self::$session->start();
		if (! isset(self::$session['email']))
			return self::_error(403,'forbidden');
		self::_send_json(_::db()->get_place_from_email(self::$session['email']));
	}

	protected static function _service_request()
	{
		if ((! isset($_POST['email'])) || ($_POST['email'] === ''))
			return self::_cond_redirect(422,$_POST['fail'] ?? null,'email missing');
		$ticket = _::db()->create_ticket(['email' => $_POST['email'], 'url' => $_POST['url'] ?? null]);
		$message = new template(_::get('request-message'),['ticket' => $ticket]);
		mail($_POST['email'],_::get('request-subject'),$message,implode("\r\n",_::get('request-headers')));
		self::_cond_redirect(200,$_POST['success'] ?? null,'message de vérification d\'adresse envoyé.');
	}

	protected static function _service_validate(string $ticket)
	{
		$data = _::db()->verify_ticket($ticket);
		if (count($data) === 0)
			return server::found(_::get('request-fail-url'));
		self::$session->start();
		self::$session['email'] = $data['email'];
		if ($data['url'] !== null)
			return server::found($data['url']);
	}

	protected static function _service_session()
	{
		echo json_encode(self::$session->exists());
	}

	protected static function _service_session_redirect()
	{
		if (self::$session->exists())
			self::_cond_redirect(200,$_POST['open'] ?? null,'session ouverte.');
		else
			self::_cond_redirect(200,$_POST['none'] ?? null,'pas de session.');
	}

	protected static function _service_record()
	{
		if (! self::$session->exists())
			return self::_cond_redirect(403,$_POST['forbidden'] ?? null);
		self::$session->start();
		if (! isset(self::$session['email']))
			return self::_cond_redirect(403,$_POST['forbidden'] ?? null);
		$type = _::get('types')[$_POST['type']] ?? null;
		if ($type === null)
			return self::_cond_redirect(422,$_POST['fail'] ?? null);
		$specials = ['type','latitude','longitude','forbidden','success','fail'];
		$user = [];
		foreach ($_POST as $key => $value)
		{
			if (array_search($key,$specials) !== false)
				continue;
			$user[$key] = $value;
		}
		$latitude = (strlen($_POST['latitude']) > 0)?round(((float)$_POST['latitude']) * 1000) % 90000:null;
		$longitude = (strlen($_POST['longitude']) > 0)?round(((float)$_POST['longitude']) * 1000) % 360000:null;
		_::db()->add_place($_POST['type'],self::$session['email'],$latitude,$longitude,$type['approve-email'] === null,$user);
		if (($type['approve-email'] !== null) && (($id = _::db()->id()) !== 0))
		{
			$ticket = _::db()->create_ticket(['approve-id' => _::db()->id()]);
			$message = new template(_::get('approve-message'),['ticket' => $ticket, 'data' => $_POST]);
			mail($type['approve-email'],_::get('approve-subject'),$message,implode("\r\n",_::get('approve-headers')));
		}
		self::_cond_redirect(200,$_POST['success'] ?? null);
	}

	protected static function _service_approve(string $ticket)
	{
		$data = _::db()->verify_ticket($ticket);
		if (count($data) === 0)
			return print('ticket introuvable');
		if ($data['approve-id'] === null)
			throw new exception('wrong ticket'); 
		_::db()->approve_place($data['approve-id']);
		$place = _::db()->get_place($data['approve-id']);
		$type = _::get('types')[$place['type']] ?? null;
		if ($type === null)
			throw new exception('unknown place type %s',$place['type'] ??  null);
		$message = new template($type['approval-message'],$place);
		$headers = $type['headers'] ?? [];
		$headers[] = sprintf('From: %s',$type['approve-email']);
		mail($place['email'],$type['approval-subject'],$message,implode("\r\n",$headers));
		return server::found(_::get('approve-url'));
	}

	protected static function _service_reject(string $ticket)
	{
		$data = _::db()->verify_ticket($ticket);
		if (count($data) === 0)
			return print('ticket introuvable');
		if ($data['approve-id'] === null)
			throw new exception('wrong ticket'); 
		_::db()->remove_place($data['approve-id']);
		return server::found(_::get('reject-url'));
	}

	protected static function _service_message()
	{
		if (! self::$session->exists())
			return self::_cond_redirect(403,$_POST['forbidden'] ?? null,'forbidden');
		self::$session->start();
		if (! isset(self::$session['email']))
			return self::_cond_redirect(403,$_POST['forbidden'] ?? null,'forbidden');
		if (! isset($_POST['id']))
			return self::_cond_redirect(422,$_POST['fail'] ?? null,'missing place id');
		$place = _::db()->get_place($_POST['id']);
		$type = _::get('types')[$place['type']] ?? null;
		if ($type === null)
			return self::_cond_redirect(422,$_POST['fail'] ?? null,'unknown type %s',$place['type'] ?? 'null');
		$message = new template($type['message'],$_POST);
		$headers = $type['headers'] ?? [];
		$headers[] = sprintf('From: %s',self::$session['email']);
		mail($place['email'],$_POST['subject'] ?? '',$message,implode("\r\n",$headers));
		self::_cond_redirect(200,$_POST['success'] ?? null,'message send');
	}

	protected static function _service_infos()
	{
		phpinfo();
	}

	public static function response(string $protocol, string $verb, string $uri)
	{
		foreach (_::get('http-headers') as $header)
			header($header);
		if (($pos = strpos($uri,'?')) !== false)
			$uri = substr($uri,0,$pos);
		$path = $uri;
		$data = null;
		if (preg_match('|^(/[^:?]+):(.*)$|',$uri,$matches))
		{
			$path = $matches[1];
			$data = urldecode($matches[2] ?? '');
		}
		if (isset(self::$services[$protocol][$verb][$path]))
		{
			self::$session = new session(
				new mysql(_::db()->base(_::get('db-base')),'map_session',_::get('session-ttl')),
				new cookie('fedmap_session',_::get('session-ttl'),$_SERVER['SERVER_NAME'],_::get('cookie-path'),false));
			$service = self::$services[$protocol][$verb][$path];
			if ($service[1] !== null)
				header('Content-Type: '.$service[1]);
			$method = $service[0];
			self::$method($data);
		}
		else
			server::not_found();
	}
}