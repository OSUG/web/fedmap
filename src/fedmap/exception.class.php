<?php

namespace fedmap;

class exception extends \Exception
{
	public function __construct(string $string,...$args)
	{
		parent::__construct(vsprintf($string,$args));
	}
}
