<?php return [
	'prefix-len' => 7, // number of caracters to remove from the start of URI paht. 
	'http-headers' => [
		'Access-Control-Allow-Origin:  http://michel',
		'Access-Control-Allow-Methods: GET, POST',
		'Access-Control-Allow-Headers: Content-Type',
		'Access-Control-Allow-Credentials: true'
	], // additional HTTP headers
	'cookie-path' => '/fedmap/', // cookie path
	'db-host' => 'localhost', // mysql host address
	'db-user' => 'map', // mysql user login
	'db-pass' => 'map', // mysql user password
	'db-base' => 'map', // mysql database name
	'session-ttl' => 3600, // seconds before session expiration
	'ticket-ttl' => 3600, // seconds before granting ticket expiration
	'request-subject' => 'Vérification adresse email', // subject of email address verification message
	'request-message' => __DIR__.'/message-template.php', // template of email address verification message
	'request-headers' => [
		'From: Alphonse <alphonse@univ-grenoble-alpes.fr>',
		'MIME-Version: 1.0',
		'Content-type: text/html; charset=iso-8859-1'
	], // additional verification email headers
	'approve-subject' => 'Approbation de l\'enregistrement', // subject of record approval message
	'approve-message' => __DIR__.'/approve-template.php', // template of record approval message
	'approve-headers' => [
		'From: Alphonse <alphonse@univ-grenoble-alpes.fr>'
	], // additional approve email headers
	'approve-url' => 'http://spip-dev.osug.fr/appouvee.html', // URL to redirect to when approved.
	'types' => [ // place's types
		[
			'approve-email' => 'michel.gravier@univ-grenoble-alpes.fr', // email address recieving record approbation message for this type
			'max-data-size' => 4096, // max data size posted en record
			'message' => __DIR__.'/formateur-message-template.php', // template of message sent to email associated with a place on map.
			'headers' => [
				'MIME-Version: 1.0',
				'Content-type: text/html; charset=utf-8'
			], // additional email headers
			'approval-subject' => 'Confirmation', // subject of approval message
			'approval-message' => __DIR__.'/animateur-message-approbation.php' // template of approval message.
		],
		[
			'approve-email' => null, // if email address is null no approbation is needed on record.
			'max-data-size' => 4096,
			'message' => __DIR__.'/organisateur-message-template.php'
		]
	]  
];